package de.superioz.sx.bukkit.event;

import de.superioz.sx.bukkit.common.npc.NPC;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created on 10.04.2016.
 */
@Getter
public class NPCUpdateEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private Player player;
	private NPC npc;

	public NPCUpdateEvent(Player player, NPC npc){
		this.player = player;
		this.npc = npc;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
