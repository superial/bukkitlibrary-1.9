package de.superioz.sx.bukkit.common;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Created on 03.04.2016.
 */
public class GameProfileBuilder {

	private static Map<String, Skin> cache = new HashMap<>();

	/**
	 * Gets a gameprofile from given name
	 *
	 * @param name The name
	 * @return The profile
	 */
	public static GameProfile get(String name, String displayName){
		if(cache.containsKey(name))
			return cache.get(name).toProfile(displayName);
		else{
			Skin skin = new Skin(name);
			cache.put(name, skin);
			return skin.toProfile(displayName);
		}
	}

	@Getter
	private static class Skin {

		String uuid;
		UUID asUUID;
		String name;
		String value;
		String signatur;
		String playername;

		/**
		 * The constructor with given uuid
		 *
		 * @param name The name
		 */
		public Skin(String name){
			UUID uuid = Bukkit.getOfflinePlayer(name).getUniqueId();
			this.uuid = uuid.toString().replaceAll("-", "");
			this.asUUID = uuid;
			load();
		}

		/**
		 * Returns the skin as profile
		 *
		 * @return The profile
		 */
		public GameProfile toProfile(String name){
			GameProfile profile = new GameProfile(getAsUUID(), name);
			profile.getProperties().clear();
			profile.getProperties().put(getSkinName(), new Property(getSkinName(), getSkinValue(), getSkinSignatur()));

			return profile;
		}

		/**
		 * Loads the skin
		 */
		private void load(){
			try{
				OfflinePlayer offP = Bukkit.getServer().getOfflinePlayer(asUUID);
				playername = offP.getName();

				URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
				URLConnection uc = url.openConnection();
				uc.setUseCaches(false);
				uc.setDefaultUseCaches(false);
				uc.addRequestProperty("User-Agent", "Mozilla/5.0");
				uc.addRequestProperty("Cache-Control", "no-cache, no-store, must-revalidate");
				uc.addRequestProperty("Pragma", "no-cache");

				// Parse it
				@SuppressWarnings("resource")
				String json = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A").next();
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(json);
				JSONArray properties = (JSONArray) ((JSONObject) obj).get("properties");
				for(Object property1 : properties){
					try{
						JSONObject property = (JSONObject) property1;
						String name = (String) property.get("name");
						String value = (String) property.get("value");
						String signature = property.containsKey("signature") ? (String) property.get("signature") : null;


						this.name = name;
						this.value = value;
						this.signatur = signature;


					}
					catch(Exception e){
						Bukkit.getLogger().log(Level.WARNING, "Failed to apply auth property", e);
					}
				}
				uc.setConnectTimeout(0);
				uc.getInputStream().close();
			}
			catch(Exception e){
				e.printStackTrace();
				throw new RuntimeException("Couldn't load skin.");
			}
		}

		public String getSkinValue(){
			return value;
		}

		public String getSkinName(){
			return name;
		}

		public String getSkinSignatur(){
			return signatur;
		}

		public String getUUID(){
			return uuid;
		}

		public UUID getAsUUID(){
			return asUUID;
		}

		public String getPlayerName(){
			return playername;
		}

	}

}
