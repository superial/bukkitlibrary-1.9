package de.superioz.sx.bukkit.common.command.verification;

import de.superioz.sx.bukkit.common.command.AllowedCommandSender;
import de.superioz.sx.bukkit.common.command.Command;
import de.superioz.sx.bukkit.common.command.CommandCase;
import de.superioz.sx.bukkit.common.command.context.CommandContext;
import de.superioz.sx.java.util.SimpleStringUtils;
import org.bukkit.entity.Player;

/**
 * Created on 10.04.2016.
 */
@Command(label = MethodVerifier.COMMAND_NAME,
		flags = {"a"},
		commandTarget = AllowedCommandSender.PLAYER)
public class MethodVerifierCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		Player player = context.getSenderAsPlayer();
		if(context.getArgumentsLength() < 1){
			return;
		}
		String arg = context.getArgument(1);

		// Check arg
		if(arg == null || arg.isEmpty()
				|| !SimpleStringUtils.isInteger(arg)){
			return;
		}
		int id = Integer.valueOf(arg);

		// Check id
		if(!MethodVerifier.has(id)){
			return;
		}

		boolean result = context.hasFlag("a");
		MethodVerifier.verified(id, result, player);
	}

}
