package de.superioz.sx.bukkit.test;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.command.CommandHandler;
import de.superioz.sx.bukkit.exception.CommandRegisterException;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public class TestPlugin extends JavaPlugin {

	@Override
	public void onEnable(){
		BukkitLibrary.initFor(this);

		try{
			CommandHandler.registerCommand(TestCommand.class, TestSubCommands.class);
		}
		catch(CommandRegisterException e){
			e.printStackTrace();
		}
	}

}
