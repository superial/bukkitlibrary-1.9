package de.superioz.sx.bukkit.test;

import de.superioz.sx.bukkit.common.ViewManager;
import de.superioz.sx.bukkit.common.command.AllowedCommandSender;
import de.superioz.sx.bukkit.common.command.Command;
import de.superioz.sx.bukkit.common.command.CommandCase;
import de.superioz.sx.bukkit.common.command.context.CommandContext;
import de.superioz.sx.bukkit.common.npc.NPCHuman;
import de.superioz.sx.bukkit.common.protocol.EnumWrappers;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created on 01.03.2016.
 */
@Command(label = "testcommand", desc = "Just a command for testing things", permission = "test.properties.command",
		usage = "<flag> <parameter>", commandTarget = AllowedCommandSender.PLAYER,
		flags = {"?"})
public class TestCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		Player sender = context.getSenderAsPlayer();
		Location loc = sender.getLocation();

		ViewManager.sendHotbarMessage("&7Spawn...", sender);

		// Code here
		NPCHuman human = new NPCHuman(loc, "&cThe NPC", new NPCHuman.Profile(null, "rewinside"));
		human.spawn(sender);

		ViewManager.sendBossbar("&bDas ist eine Bossbar!", UUID.randomUUID(), EnumWrappers.BossbarColor.BLUE,
				EnumWrappers.BossbarStyle.PROGRESS, 1F,
				EnumWrappers.BossbarAction.ADD, sender);
	}

}
